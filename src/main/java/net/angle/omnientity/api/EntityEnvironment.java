package net.angle.omnientity.api;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import net.angle.omnientity.impl.BasicModifyComponentDatagramAction;
import net.angle.omnientity.impl.BasicEntityEnvironmentDatagram;
import net.angle.omnientity.impl.BasicGetEntityDatagramAction;
import net.angle.omninetwork.api.Datagram;
import net.angle.omniregistry.api.Registry;
import net.angle.omnirendering.api.RenderComponent;
import net.angle.omnirendering.api.RenderTemplate;
import net.angle.omniserialization.api.ClassIDRegistry;

/**
 *
 * @author angle
 */
public interface EntityEnvironment extends Registry<Entity> {
    
    public default <T extends SteppableComponent> void step(List<Class<T>> componentClasses, float dt) {
        componentClasses.forEach((t) -> {
            step(t, dt);
        });
    }
    public default <T extends SteppableComponent> void step(Class<T> componentClasses, float dt) {
        getComponents(componentClasses).forEach((t) -> {
            t.step(dt);
        });
    }
    
    public default <T extends SteppableComponent> void step(float dt) {
        step(SteppableComponent.class, dt);
    }
    public ClassIDRegistry getComponentInterfaceRegistry();
    public ClassIDRegistry getComponentClassRegistry();
    public void registerTemplate(RenderTemplate template);
    public RenderComponent getRenderComponent(int componentID, Object[] info);
    public Map<Class<? extends Component>, Function<Entity, Component>> getComponentConstructors();
    public <T extends Component> void addComponent(T component);
    public default void addComponents(Collection<Component> components) {
        components.forEach((component) -> {
            addComponent(component);
        });
    }
    
    public <T extends Component> List<T> getComponents(Class<T> def);
    
    public default void sendUpdates(Class<? extends UpdateableComponent> componentInterface, Consumer<Datagram> sendMethod) {
        getComponents(componentInterface).forEach((component) -> {
            if (!component.hasUpdates())
                return;
            BasicEntityEnvironmentDatagram datagram = new BasicEntityEnvironmentDatagram(
                    new BasicGetEntityDatagramAction(component.getEntity().getId(),
                            new BasicModifyComponentDatagramAction(getComponentInterfaceRegistry().getID(componentInterface), component.getUpdate())), 0);
            sendMethod.accept(datagram);
            component.setHasUpdates(false);
        });
    }
    
    public default void destroy() {
        getAllEntries().forEach((t) -> {
            t.destroy();
        });
    }
}