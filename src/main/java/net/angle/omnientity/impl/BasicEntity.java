package net.angle.omnientity.impl;

import lombok.Getter;
import net.angle.omnientity.api.EntityEnvironment;

/**
 *
 * @author angle
 */
public class BasicEntity extends AbstractEntity {
    private final @Getter EntityEnvironment entityEnvironment;
    
    public BasicEntity(int id, EntityEnvironment entityEnvironment) {
        super(id, entityEnvironment);
        this.entityEnvironment = entityEnvironment;
    }
    
    public BasicEntity(EntityEnvironment entityEnvironment) {
        super(entityEnvironment);
        this.entityEnvironment = entityEnvironment;
    }

    @Override
    public void destroy() {
        getComponents().forEach((component) -> {
            component.destroy();
        });
    }

    @Override
    public String toString() {
        return "Entity(id: " + getId() + ", num components: " + getComponents().size() + ")";
    }
}