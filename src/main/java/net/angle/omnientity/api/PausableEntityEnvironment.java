package net.angle.omnientity.api;

/**
 *
 * @author angle
 */
public interface PausableEntityEnvironment extends EntityEnvironment {
    public boolean isPaused();
    public void pause();
    public void unpause();

    @Override
    public default <T extends SteppableComponent> void step(float dt) {
        if (isPaused())
            step(UnpausableComponent.class, dt);
        else
            EntityEnvironment.super.step(dt);
    }
    
}