package net.angle.omnientity.impl;

import com.samrj.devil.gl.ShaderProgram;
import com.samrj.devil.math.Mat4;
import java.util.Arrays;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;
import net.angle.omnientity.api.Entity;
import net.angle.omnientity.api.PositionComponent;
import net.angle.omnientity.api.RenderingComponent;
import net.angle.omnirendering.api.RenderComponent;

/**
 *
 * @author angle
 */
public class BasicRenderingComponent extends AbstractUpdateableComponent<Object[]> implements RenderingComponent {
    private @Getter RenderComponent renderComponent = null;
    private @Setter Object[] renderingData;

    public BasicRenderingComponent(Entity entity) {
        super(entity);
    }

    public void setRenderComponent(RenderComponent renderComponent) {
        if (Objects.nonNull(this.renderComponent))
            this.renderComponent.destroy(false);
        this.renderComponent = renderComponent;
    }
    
    @Override
    public void render(ShaderProgram shader) {
        if (Objects.nonNull(renderComponent)) {
            prepShader(shader);
            renderComponent.render(shader);
        }
    }
    
    @Override
    public void prepShader(ShaderProgram shader) {
        shader.uniformMat4("u_model_matrix", Mat4.translation(getEntity().getComponent(PositionComponent.class).getPosition()));
    }

    @Override
    public void update(Object[] update) {
        setRenderingData(update);
        setRenderComponent(getEntity().getEntityEnvironment().getRenderComponent((int) update[0], (Object[]) update[1]));
    }

    @Override
    public Object[] getUpdate() {
        return renderingData;
    }

    @Override
    public void destroy() {
        super.destroy();
        if (Objects.nonNull(renderComponent))
            renderComponent.destroy(false);
    }

    @Override
    public String toString() {
        return "Rendering(RenderComponent: " + renderComponent + ", Data: " + Arrays.toString((Object[]) renderingData[1]) + ")";
    }
}