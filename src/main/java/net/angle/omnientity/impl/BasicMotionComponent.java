package net.angle.omnientity.impl;

import com.samrj.devil.math.Vec3;
import java.util.Objects;
import net.angle.omnientity.api.CollisionComponent;
import net.angle.omnientity.api.Entity;
import net.angle.omnientity.api.MotionComponent;
import net.angle.omnientity.api.PositionComponent;

/**
 *
 * @author angle
 */
public class BasicMotionComponent extends AbstractUpdateableComponent<Vec3> implements MotionComponent {
    private final Vec3 vector = new Vec3();
    private PositionComponent positionComponent;
    private CollisionComponent collisionComponent;

    public BasicMotionComponent(Entity entity) {
        super(entity);
    }

    @Override
    public void init() {
        super.init();
        positionComponent = getEntity().getComponent(PositionComponent.class);
        collisionComponent = getEntity().getComponent(CollisionComponent.class);
    }
    
    @Override
    public Vec3 getVector() {
        return new Vec3(vector);
    }

    @Override
    public void setVector(Vec3 vec) {
        vector.set(vec);
        setHasUpdates(true);
    }

    @Override
    public void addToVector(Vec3 addition) {
        vector.add(addition);
        setHasUpdates(true);
    }

    @Override
    public void step(float dt) {
        Vec3 vec = getVector();
        if (!vec.isZero()) {
            
            if (Objects.nonNull(collisionComponent)) {
                vec = collisionComponent.check(new Vec3(positionComponent.getPosition()), vec, dt);
                positionComponent.setPosition(vec);
            } else {
                vec.mult(dt);
                positionComponent.addToPosition(vec);
            }
        }
    }

    @Override
    public String toString() {
        return "Motion(" + getVector()+ ")";
    }
}