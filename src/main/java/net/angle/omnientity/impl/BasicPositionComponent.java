package net.angle.omnientity.impl;

import com.samrj.devil.math.Vec3;
import net.angle.omnientity.api.Entity;
import net.angle.omnientity.api.PositionComponent;

/**
 *
 * @author angle
 */
public class BasicPositionComponent extends AbstractUpdateableComponent<Vec3> implements PositionComponent {
    private final Vec3 position = new Vec3();

    public BasicPositionComponent(Entity entity) {
        super(entity);
    }

    @Override
    public void init() {
        super.init();
    }
    
    @Override
    public Vec3 getPosition() {
        return new Vec3(position);
    }

    @Override
    public void setPosition(Vec3 newPosition) {
        position.set(newPosition);
        setHasUpdates(true);
    }

    @Override
    public void addToPosition(Vec3 addition) {
        position.add(addition);
        setHasUpdates(true);
    }

    @Override
    public String toString() {
        return "Position(" + getPosition() + ")";
    }
}