/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.angle.omnientity.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import net.angle.omnientity.api.Component;
import net.angle.omnientity.api.Entity;
import net.angle.omniregistry.impl.AbstractRegistryObject;
import net.angle.omnientity.api.EntityEnvironment;

/**
 *
 * @author angle
 */
public abstract class AbstractEntity extends AbstractRegistryObject implements Entity {
    private final Map<Class<? extends Component>, List<Component>> componentLists = new HashMap<>();
    private boolean initialized = false;
    private final EntityEnvironment environment;
    
    public AbstractEntity(EntityEnvironment entityCollection) {
        this(entityCollection.getNextEntryID(), entityCollection);
    }
    
    public AbstractEntity(int id, EntityEnvironment environment) {
        super(id, environment.getId());
        this.environment = environment;
    }

    @Override
    public boolean isInitialized() {
        return initialized;
    }
    
    @Override
    public void init() {
        Entity.super.init();
        initialized = true;
    }

    @Override
    public boolean hasComponent(Class<? extends Component> def) {
        List<Component> list = componentLists.get(def);
        return Objects.nonNull(list) && !list.isEmpty();
    }

    @Override
    public <T extends Component> List<T> getComponentList(Class<T> componentClass) {
        List<T> componentList = (List<T>) componentLists.get(componentClass);
        if (Objects.isNull(componentList)) {
            componentList = new ArrayList<>();
            componentLists.put(componentClass, (List<Component>) componentList);
        }
        return componentList;
    }

    @Override
    public <T extends Component> List<T> getComponentList(int id) {
        return getComponentList(environment.getComponentInterfaceRegistry().getClass(id));
    }

    @Override
    public <T extends Component> T addComponent(T comp) {
        for (Class<? extends Component> componentClass : comp.getComponentInterfaceList()) {
            List<Component> list = (List<Component>) getComponentList(componentClass);
            list.add(comp);
        }
        return comp;
    }

    @Override
    public <T extends Component> T getComponent(Class<T> cls) {
        List<T> componentList = getComponentList(cls);
        if (componentList.isEmpty())
            return null;
        else return (T) componentList.get(0);
    }

    @Override
    public <T extends Component> T getComponent(int id) {
        return (T) getComponent(environment.getComponentInterfaceRegistry().getClass(id));
    }
}