package net.angle.omnientity.api;

import com.samrj.devil.gl.ShaderProgram;
import com.samrj.devil.graphics.Camera3D;
import com.samrj.devil.graphics.Camera3DController;
import com.samrj.devil.graphics.ViewFrustum;
import com.samrj.devil.math.Mat4;
import java.util.List;
import com.google.common.collect.ImmutableList;
/**
 *
 * @author angle
 */
public interface CameraComponent extends SteppableComponent {
    public Camera3D getCamera();
    public ViewFrustum getFrustum();
    public Camera3DController getCameraController();
    public Mat4 getViewProjMat();
    public void prepShader(ShaderProgram shader);

    @Override
    public default List<Class<? extends Component>> getComponentInterfaceList() {
        return ImmutableList.of(CameraComponent.class, SteppableComponent.class, Component.class);
    }
}