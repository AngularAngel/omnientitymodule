package net.angle.omnientity.api;

import com.samrj.devil.math.Vec2;

/**
 *
 * @author angle
 */
public interface PlayerControllerComponent extends ControllerComponent {
    public void updateMouse(Vec2 mousePos);
    public void mouseMoved(Vec2 mousePos);
}
