package net.angle.omnientity.impl;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.angle.omnientity.api.Entity;
import net.angle.omninetwork.api.DatagramAction;
import net.angle.omnientity.api.GetEntityDatagramAction;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class BasicGetEntityDatagramAction implements GetEntityDatagramAction {
    private final @Getter int entityID;
    private final @Getter DatagramAction<Entity>[] actions;

    public BasicGetEntityDatagramAction(int entityID, DatagramAction<Entity> action) {
        this(entityID, new DatagramAction[]{action});
    }
}
