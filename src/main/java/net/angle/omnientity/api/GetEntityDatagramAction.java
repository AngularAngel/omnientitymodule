package net.angle.omnientity.api;

import net.angle.omninetwork.api.DatagramAction;
import net.angle.omninetwork.api.ActionList;

/**
 *
 * @author angle
 */
public interface GetEntityDatagramAction extends DatagramAction<EntityEnvironment>, ActionList<Entity> {
    public int getEntityID();

    @Override
    public default void execute(EntityEnvironment environment) {
        Entity entity = environment.getEntryById(getEntityID());
        for (DatagramAction<Entity> action : getActions())
            action.execute(entity);
    }
}