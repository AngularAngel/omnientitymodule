package net.angle.omnientity.api;

/**
 *
 * @author angle
 */
public interface CreateComponentDatagramAction extends ComponentDatagramAction {

    @Override
    public default void execute(Entity entity) {
        EntityEnvironment entityEnvironment = entity.getEntityEnvironment();
        Class<Component> cls = entityEnvironment.getComponentClassRegistry().getClass(getComponentID());
        entity.addComponent(entityEnvironment.getComponentConstructors().get(cls).apply(entity));
    }
}