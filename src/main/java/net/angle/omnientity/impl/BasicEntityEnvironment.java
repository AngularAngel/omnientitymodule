package net.angle.omnientity.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import lombok.Getter;
import net.angle.omnientity.api.Component;
import net.angle.omnientity.api.Entity;
import net.angle.omniregistry.api.Registry;
import net.angle.omniregistry.impl.BasicRegistry;
import net.angle.omnientity.api.EntityEnvironment;
import net.angle.omnirendering.api.RenderComponent;
import net.angle.omnirendering.api.RenderTemplate;
import net.angle.omniserialization.api.ClassIDRegistry;
import net.angle.omniserialization.impl.BasicClassIDRegistry;

/**
 *
 * @author angle
 */
public class BasicEntityEnvironment extends BasicRegistry<Entity> implements EntityEnvironment {
    private final Map<Class<? extends Component>, List<Component>> components = new HashMap<>();
    private final @Getter ClassIDRegistry componentInterfaceRegistry = new BasicClassIDRegistry();
    private final @Getter ClassIDRegistry componentClassRegistry = new BasicClassIDRegistry();
    private final @Getter Map<Class<? extends Component>, Function<Entity, Component>> componentConstructors = new HashMap<>();
    private final List<RenderTemplate> renderTeplates = new ArrayList<>();
    
    public BasicEntityEnvironment(String name, Registry<? extends Registry> registry) {
        super(name, registry);
    }

    @Override
    public <T extends Component> void addComponent(T component) {
        for (Class<? extends Component> componentClass : component.getComponentInterfaceList()) {
            List<Component> list = (List<Component>) getComponents(componentClass);
            list.add((Component) component);
        }
    }

    @Override
    public <T extends Component> List<T> getComponents(Class<T> def) {
        List<T> componentList = (List<T>) components.get(def);
        if (Objects.isNull(componentList)) {
            componentList = new ArrayList<>();
            components.put(def, (List<Component>) componentList);
        }
        return componentList;
    }

    @Override
    public <E extends Entity> E addEntry(E e) {
        if (!e.isInitialized())
            e.init();
        addComponents(e.getComponents());
        return super.addEntry(e);
    }

    @Override
    public void registerTemplate(RenderTemplate template) {
        renderTeplates.add(template);
    }

    @Override
    public RenderComponent getRenderComponent(int componentID, Object[] info) {
        return renderTeplates.get(componentID).getComponent(info);
    }
}