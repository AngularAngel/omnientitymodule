package net.angle.omnientity.impl;

import com.samrj.devil.game.GameWindow;
import com.samrj.devil.math.Vec2;
import com.samrj.devil.math.Vec3;
import net.angle.omnientity.api.CameraComponent;
import net.angle.omnientity.api.Entity;
import net.angle.omnientity.api.MotionComponent;
import org.lwjgl.glfw.GLFW;
import net.angle.omnientity.api.PlayerControllerComponent;

/**
 *
 * @author angle
 */
public class BasicPlayerControllerComponent extends AbstractComponent implements PlayerControllerComponent {
    private MotionComponent motion;
    private CameraComponent camera;
    private final Vec2 prevMouse = new Vec2();

    public BasicPlayerControllerComponent(Entity entity) {
        super(entity);
    }
    
    @Override
    public void updateMouse(Vec2 mousePos) {
        prevMouse.set(mousePos);
    }

    @Override
    public void init() {
        super.init();
        motion = getEntity().getComponent(MotionComponent.class);
        camera = getEntity().getComponent(CameraComponent.class);
        
        updateMouse(GameWindow.getMouse().getPos());
    }
    
    @Override
    public void mouseMoved(Vec2 mousePos) {
        float dx = mousePos.x - prevMouse.x;
        float dy = mousePos.y - prevMouse.y;
        camera.getCameraController().mouseDelta(dx, dy);
        updateMouse(mousePos);
    }
    
    public Vec3 calculateMovementDirection() {
        float forwards = 0, rightwards = 0;
        
        if (GameWindow.getKeyboard().isKeyDown(GLFW.GLFW_KEY_W)){
            forwards = 1;
        } else if (GameWindow.getKeyboard().isKeyDown(GLFW.GLFW_KEY_S)){
            forwards = -1;
        }

        if (GameWindow.getKeyboard().isKeyDown(GLFW.GLFW_KEY_A)){
            rightwards = -1;
        }else if (GameWindow.getKeyboard().isKeyDown(GLFW.GLFW_KEY_D)){
            rightwards = 1;
        }
        
        float camSin = (float) Math.sin(camera.getCameraController().getYaw());
        float camCos = (float) Math.cos(camera.getCameraController().getYaw());
        Vec3 flatForward = new Vec3(-camSin, 0.0f, -camCos);
        Vec3 flatRight   = new Vec3(camCos, 0.0f, -camSin);
        Vec3 movementDirection = Vec3.mult(flatRight, rightwards);
        movementDirection.madd(flatForward, forwards);
        
        if (GameWindow.getKeyboard().isKeyDown(GLFW.GLFW_KEY_LEFT_SHIFT)) {
            movementDirection.y = -1;
        }

        if (GameWindow.getKeyboard().isKeyDown(GLFW.GLFW_KEY_SPACE)) {
            movementDirection.y = 1;
        }
        
        if (movementDirection.length() != 0)
            movementDirection.normalize();
        return movementDirection;
    }
    
    @Override
    public void step(float dt) {
        motion.setVector(calculateMovementDirection().mult(10));
    }

    @Override
    public String toString() {
        return "PlayerController(PrevMouse: " + prevMouse + ")";
    }
}