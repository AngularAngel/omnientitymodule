package net.angle.omnientity.api;

import com.google.common.collect.ImmutableList;
import java.util.List;

/**
 *
 * @author angle
 */
public interface SteppableComponent extends Component {
    @Override
    public default List<Class<? extends Component>> getComponentInterfaceList() {
        return ImmutableList.of(SteppableComponent.class, Component.class);
    }
    public void step(float dt);
}