package net.angle.omnientity.api;

import java.util.List;

/**
 *
 * @author angle
 */
public interface Component {
    public List<Class<? extends Component>> getComponentInterfaceList();
    
    public default void init() {};
    
    public Entity getEntity();
    
    public default void destroy() {}
}