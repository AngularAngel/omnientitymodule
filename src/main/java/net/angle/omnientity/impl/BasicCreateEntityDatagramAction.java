package net.angle.omnientity.impl;

import net.angle.omnientity.api.CreateEntityDatagramAction;
import net.angle.omnientity.api.Entity;
import net.angle.omnientity.api.EntityEnvironment;
import net.angle.omninetwork.api.DatagramAction;

/**
 *
 * @author angle
 */
public class BasicCreateEntityDatagramAction extends BasicGetEntityDatagramAction implements CreateEntityDatagramAction {
    public BasicCreateEntityDatagramAction(int entityID, DatagramAction<Entity>[] actions) {
        super(entityID, actions);
    }
    
    public BasicCreateEntityDatagramAction(int entityID, DatagramAction<Entity> action) {
        super(entityID, action);
    }
    
    @Override
    public void execute(EntityEnvironment entityEnvironment) {
        CreateEntityDatagramAction.super.execute(entityEnvironment);
    }
}