package net.angle.omnientity.impl;

import java.lang.reflect.Constructor;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.NonNull;
import net.angle.omnientity.api.CameraComponent;
import net.angle.omnientity.api.Component;
import net.angle.omnientity.api.ControllerComponent;
import net.angle.omnientity.api.Entity;
import net.angle.omnientity.api.EntityEnvironment;
import net.angle.omnientity.api.MotionComponent;
import net.angle.omnientity.api.OmniEntityModule;
import net.angle.omnientity.api.PositionComponent;
import net.angle.omnientity.api.RenderingComponent;
import net.angle.omnimodule.impl.BasicOmniModule;
import net.angle.omninetwork.api.DatagramAction;
import net.angle.omninetwork.impl.OmniNetworkModuleImpl;
import net.angle.omniregistry.api.RegistryCollection;
import net.angle.omniserialization.api.Builder;
import net.angle.omniserialization.api.ClassIDRegistry;
import net.angle.omniserialization.api.ObjectSerializerRegistry;
import net.angle.omniserialization.impl.ReflectiveBuilder;
import net.angle.omniserialization.impl.ReflectiveSerializer;

/**
 *
 * @author angle
 */
public class OmniEntityModuleImpl extends BasicOmniModule implements OmniEntityModule {
    public OmniEntityModuleImpl() {
        super(OmniEntityModule.class);
    }
    
    @Override
    public void populateRegistries(RegistryCollection registries) {
    }
    
    @Override
    public Entity getClientPlayer(int id, EntityEnvironment entityCollection) {
        BasicEntity player = new BasicEntity(id, entityCollection);
        player.addComponent(new BasicPositionComponent(player));
        player.addComponent(new BasicMotionComponent(player));
        player.addComponent(new BasicCameraComponent(player));
        player.addComponent(new BasicPlayerControllerComponent(player));
        player.addComponent(new BasicRenderingComponent(player));
        return player;
    }
    
    @Override
    public void prepSerializerRegistry(ObjectSerializerRegistry registry) {
        try {
            registry.registerObject(BasicEntityEnvironmentDatagram.class.getConstructor(DatagramAction[].class, int.class), (t) -> {
                return new Object[]{t.getActions(), t.getPriority()};
            });
            
            registry.registerObject(BasicGetEntityDatagramAction.class.getConstructor(int.class, DatagramAction[].class), (t) -> {
                return new Object[]{t.getEntityID(), t.getActions()};
            });
            
            registry.registerObject(BasicCreateEntityDatagramAction.class.getConstructor(int.class, DatagramAction[].class), (t) -> {
                return new Object[]{t.getEntityID(), t.getActions()};
            });
            
            registry.registerObject(BasicCreateComponentDatagramAction.class.getConstructor(int.class), (t) -> {
                return new Object[]{t.getComponentID()};
            });
            
            registry.registerObject(BasicModifyComponentDatagramAction.class, new ReflectiveSerializer<>((Constructor<BasicModifyComponentDatagramAction>) BasicModifyComponentDatagramAction.class.getConstructor(int.class, Object.class), (BasicModifyComponentDatagramAction datagram) -> {
                Object fieldValue = datagram.getFieldValue();
                return new Object[]{
                    datagram.getComponentID(),
                    registry.getComponentID(fieldValue.getClass()),
                    fieldValue};
            }) {
                @Override
                public void readComponents(@NonNull ObjectSerializerRegistry registry, @NonNull ReflectiveBuilder<BasicModifyComponentDatagramAction> builder, ArrayList<Builder> builders, @NonNull ByteBuffer buffer) {
                    builder.setArgument(0, registry.readPrimitive(int.class, buffer));
                    builder.setArgument(1, registry.readComponent(registry.readPrimitive(int.class, buffer), builders, buffer));
                }
            });
        } catch (NoSuchMethodException | SecurityException ex) {
            Logger.getLogger(OmniNetworkModuleImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void prepComponentInterfaceRegistry(ClassIDRegistry registry) {
        registry.addClass(PositionComponent.class);
        registry.addClass(MotionComponent.class);
        registry.addClass(RenderingComponent.class);
    }

    @Override
    public void prepComponentClassRegistry(ClassIDRegistry registry) {
        registry.addClass(BasicPositionComponent.class);
        registry.addClass(BasicMotionComponent.class);
        registry.addClass(BasicRenderingComponent.class);
    }

    @Override
    public void prepComponentConstructors(Map<Class<? extends Component>, Function<Entity, Component>> componentConstructors) {
        componentConstructors.put(BasicPositionComponent.class, BasicPositionComponent::new);
        componentConstructors.put(BasicMotionComponent.class, BasicMotionComponent::new);
        componentConstructors.put(BasicCameraComponent.class, BasicCameraComponent::new);
        componentConstructors.put(BasicPlayerControllerComponent.class, BasicPlayerControllerComponent::new);
        componentConstructors.put(BasicRenderingComponent.class, BasicRenderingComponent::new);
    }
}