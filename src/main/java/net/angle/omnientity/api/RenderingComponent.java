package net.angle.omnientity.api;

import com.google.common.collect.ImmutableList;
import com.samrj.devil.gl.ShaderProgram;
import java.util.List;

/**
 *
 * @author angle
 */
public interface RenderingComponent extends UpdateableComponent<Object[]> {
    @Override
    public default List<Class<? extends Component>> getComponentInterfaceList() {
        return ImmutableList.of(RenderingComponent.class, UpdateableComponent.class, Component.class);
    }
    
    public void prepShader(ShaderProgram shader);
    
    public void render(ShaderProgram shader);
    public void setRenderingData(Object[] data);
}