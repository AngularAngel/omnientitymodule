package net.angle.omnientity.impl;

import lombok.Setter;
import net.angle.omnientity.api.Entity;
import net.angle.omnientity.api.UpdateableComponent;

/**
 *
 * @author angle
 */
public abstract class AbstractUpdateableComponent<T> extends AbstractComponent implements UpdateableComponent<T> {
    private @Setter boolean hasUpdates = false;

    public AbstractUpdateableComponent(Entity entity) {
        super(entity);
    }
    
    @Override
    public boolean hasUpdates() {
        return hasUpdates;
    }
}