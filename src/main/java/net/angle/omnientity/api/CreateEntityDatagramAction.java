package net.angle.omnientity.api;

import net.angle.omnientity.impl.BasicEntity;

/**
 *
 * @author angle
 */
public interface CreateEntityDatagramAction extends GetEntityDatagramAction {

    @Override
    public default void execute(EntityEnvironment entityEnvironment) {
        Entity entity =  new BasicEntity(getEntityID(), entityEnvironment);
        executeActions(entity);
        entityEnvironment.addEntry(entity);
    }
}