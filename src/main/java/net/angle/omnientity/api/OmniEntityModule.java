package net.angle.omnientity.api;

import java.util.Map;
import java.util.function.Function;
import net.angle.omnimodule.api.OmniModule;
import net.angle.omniregistry.api.RegistryCollection;
import net.angle.omniserialization.api.ClassIDRegistry;
import net.angle.omniserialization.api.ObjectSerializerRegistry;

/**
 *
 * @author angle
 */
public interface OmniEntityModule extends OmniModule {
    public void populateRegistries(RegistryCollection registries);
    public Entity getClientPlayer(int id, EntityEnvironment entityCollection);
    public void prepSerializerRegistry(ObjectSerializerRegistry registry);
    public void prepComponentInterfaceRegistry(ClassIDRegistry registry);
    public void prepComponentClassRegistry(ClassIDRegistry registry);
    public void prepComponentConstructors(Map<Class<? extends Component>, Function<Entity, Component>> componentConstructors);
}