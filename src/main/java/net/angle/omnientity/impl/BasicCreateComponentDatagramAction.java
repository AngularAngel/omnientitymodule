package net.angle.omnientity.impl;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.angle.omnientity.api.CreateComponentDatagramAction;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class BasicCreateComponentDatagramAction implements CreateComponentDatagramAction {
    private final @Getter int componentID;
}