package net.angle.omnientity.api;

import com.google.common.collect.ImmutableList;
import com.samrj.devil.math.Vec3;
import java.util.List;

/**
 *
 * @author angle
 */
public interface PositionComponent extends UpdateableComponent<Vec3> {
    @Override
    public default List<Class<? extends Component>> getComponentInterfaceList() {
        return ImmutableList.of(PositionComponent.class, UpdateableComponent.class, Component.class);
    }
    
    public void setPosition(Vec3 newPosition);
    public Vec3 getPosition();
    public void addToPosition(Vec3 addition);

    @Override
    public default void update(Vec3 update) {
        setPosition(update);
    }

    @Override
    public default Vec3 getUpdate() {
        return getPosition();
    }
    
}