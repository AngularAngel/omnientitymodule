package net.angle.omnientity.api;

import com.google.common.collect.ImmutableList;
import java.util.List;

/**
 *
 * @author angle
 */
public interface UnpausableComponent extends SteppableComponent {
    @Override
    public default List<Class<? extends Component>> getComponentInterfaceList() {
        return ImmutableList.of(UnpausableComponent.class, SteppableComponent.class, Component.class);
    }
}