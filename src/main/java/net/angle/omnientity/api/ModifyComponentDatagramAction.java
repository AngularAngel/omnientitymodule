package net.angle.omnientity.api;

/**
 *
 * @author angle
 */
public interface ModifyComponentDatagramAction extends ComponentDatagramAction {
    public Object getFieldValue();
    
    @Override
    public default void execute(Entity target) {
        ((UpdateableComponent) target.getComponent(getComponentID())).update(getFieldValue());
    }
}