package net.angle.omnientity.impl;

import lombok.Getter;
import net.angle.omnientity.api.PausableEntityEnvironment;
import net.angle.omniregistry.api.Registry;

/**
 *
 * @author angle
 */
public class BasicPausableEntityEnvironment extends BasicEntityEnvironment implements PausableEntityEnvironment {
    private @Getter boolean paused = false;

    public BasicPausableEntityEnvironment(String name, Registry<? extends Registry> registry) {
        super(name, registry);
    }

    @Override
    public void pause() {
        paused = true;
    }

    @Override
    public void unpause() {
        paused = false;
    }
}
