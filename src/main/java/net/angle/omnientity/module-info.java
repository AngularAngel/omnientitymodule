module omni.entity {
    requires static lombok;
    requires devil.util;
    requires org.lwjgl.glfw;
    requires java.logging;
    requires omni.module;
    requires omni.network;
    requires omni.registry;
    requires omni.serialization;
    requires omni.rendering;
    requires com.google.common;
    exports net.angle.omnientity.api;
    exports net.angle.omnientity.impl;
}