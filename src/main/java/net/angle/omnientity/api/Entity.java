package net.angle.omnientity.api;

import java.util.List;
import net.angle.omniregistry.api.RegistryObject;

/**
 *
 * @author angle
 */
public interface Entity extends RegistryObject {
    public EntityEnvironment getEntityEnvironment();
    public <T extends Component> T addComponent(T comp);
    
    public boolean hasComponent(Class<? extends Component> def);
    public <T extends Component> T getComponent(Class<T> def);
    public <T extends Component> T getComponent(int id);
    public <T extends Component> List<T> getComponentList(Class<T> def);
    public <T extends Component> List<T> getComponentList(int id);
    public default List<Component> getComponents() {
        return getComponentList(Component.class);
    }
    
    public boolean isInitialized();
    
    public default void init() {
        for (Component c : getComponents())
            c.init();
    }
    
    public default void destroy() {
        for (Component c : getComponents())
            c.destroy();
    }
}