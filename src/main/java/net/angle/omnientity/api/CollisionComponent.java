package net.angle.omnientity.api;

import com.google.common.collect.ImmutableList;
import com.samrj.devil.math.Vec3;
import java.util.List;

/**
 *
 * @author angle
 */
public interface CollisionComponent extends Component {

    @Override
    public default List<Class<? extends Component>> getComponentInterfaceList() {
        return ImmutableList.of(CollisionComponent.class, Component.class);
    }
    
    public Vec3 check(Vec3 start, Vec3 movement, float dt);
}