package net.angle.omnientity.impl;

import com.samrj.devil.game.GameWindow;
import com.samrj.devil.graphics.Camera3D;
import com.samrj.devil.graphics.Camera3DController;
import com.samrj.devil.gl.ShaderProgram;
import com.samrj.devil.graphics.ViewFrustum;
import com.samrj.devil.math.Mat4;
import com.samrj.devil.math.Util;
import com.samrj.devil.math.Vec2i;
import lombok.Getter;
import net.angle.omnientity.api.CameraComponent;
import net.angle.omnientity.api.Entity;
import net.angle.omnientity.api.PositionComponent;
/**
 *
 * @author angle
 */
public class BasicCameraComponent extends AbstractComponent implements CameraComponent {
    public static final float CAMERA_NEAR_Z = 0.125f;
    public static final float CAMERA_FAR_Z = 1024.0f;
    public static final float CAMERA_FOV = Util.toRadians(90.0f);
    private PositionComponent position;
    
    public final @Getter Camera3D camera;
    public final @Getter ViewFrustum frustum;
    private final @Getter Camera3DController cameraController;

    public BasicCameraComponent(float camera_near_z, float camera_far_z, float camera_fov, Entity entity) {
        super(entity);
        camera = new Camera3D();
        frustum = new ViewFrustum(camera_near_z, camera_far_z, camera_fov, 1.0f);

        Vec2i resolution = GameWindow.getResolution();
        frustum.setFOV(resolution.x, resolution.y, camera_fov);
        
        cameraController = new Camera3DController(camera);
    }
    
    public BasicCameraComponent(Entity entity) {
        this(CAMERA_NEAR_Z, CAMERA_FAR_Z, CAMERA_FOV, entity);
    }

    @Override
    public void init() {
        super.init();
        position = getEntity().getComponent(PositionComponent.class);
    }
    
    @Override
    public void step(float dt) {
        cameraController.target.set(position.getPosition());
        cameraController.update();
    }
    
    @Override
    public void prepShader(ShaderProgram shader) {
        shader.uniformMat4("u_projection_matrix", frustum.projMat);
        shader.uniformMat4("u_view_matrix", camera.viewMat);
        shader.uniform1f("u_z_far", frustum.zFar);
    }

    @Override
    public Mat4 getViewProjMat() {
        return Mat4.mult(frustum.projMat, camera.viewMat);
    }

    @Override
    public String toString() {
        return "Camera(Camera: " + getCamera().forward + ", Frustum: " + getFrustum().projMat + ")";
    }
}