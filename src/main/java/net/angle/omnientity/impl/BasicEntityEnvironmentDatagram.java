package net.angle.omnientity.impl;

import net.angle.omnientity.api.EntityEnvironment;
import net.angle.omninetwork.api.DatagramAction;
import net.angle.omninetwork.impl.AbstractActionListDatagram;

/**
 *
 * @author angle
 */
public class BasicEntityEnvironmentDatagram extends AbstractActionListDatagram<EntityEnvironment> {

    public BasicEntityEnvironmentDatagram(DatagramAction<EntityEnvironment>[] actions, int priority) {
        super(actions, priority);
    }

    public BasicEntityEnvironmentDatagram(DatagramAction<EntityEnvironment> action, int priority) {
        this(new DatagramAction[]{action}, priority);
    }
    
    @Override
    public Class<EntityEnvironment> getTargetClass() {
        return EntityEnvironment.class;
    }

    @Override
    public void execute(EntityEnvironment entityEnvironment) {
        executeActions(entityEnvironment);
    }
}