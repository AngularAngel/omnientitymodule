package net.angle.omnientity.api;

import com.google.common.collect.ImmutableList;
import java.util.List;

/**
 *
 * @author angle
 */
public interface TimerComponent extends SteppableComponent {
    @Override
    public default List<Class<? extends Component>> getComponentInterfaceList() {
        return ImmutableList.of(TimerComponent.class, SteppableComponent.class, Component.class);
    }
    
    public void addTime(float time);
    public float getTime();

    @Override
    public default void step(float dt) {
        addTime(dt);
    }
    
    
}
