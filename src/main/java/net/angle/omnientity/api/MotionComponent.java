package net.angle.omnientity.api;

import com.google.common.collect.ImmutableList;
import com.samrj.devil.math.Vec3;
import java.util.List;

/**
 *
 * @author angle
 */
public interface MotionComponent extends SteppableComponent, UpdateableComponent<Vec3> {
    @Override
    public default List<Class<? extends Component>> getComponentInterfaceList() {
        return ImmutableList.of(MotionComponent.class, SteppableComponent.class, UpdateableComponent.class, Component.class);
    }
    
    public void setVector(Vec3 vector);
    public Vec3 getVector();
    public void addToVector(Vec3 addition);

    @Override
    public default void update(Vec3 update) {
        setVector(update);
    }

    @Override
    public default Vec3 getUpdate() {
        return getVector();
    }
}