package net.angle.omnientity.impl;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.angle.omnientity.api.Component;
import net.angle.omnientity.api.Entity;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public abstract class AbstractComponent implements Component {
    private final @Getter Entity entity;
}