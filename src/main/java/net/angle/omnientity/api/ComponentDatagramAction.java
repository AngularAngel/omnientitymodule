package net.angle.omnientity.api;

import net.angle.omninetwork.api.DatagramAction;

/**
 *
 * @author angle
 */
public interface ComponentDatagramAction extends DatagramAction<Entity> {
    public int getComponentID();
}
