package net.angle.omnientity.impl;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import net.angle.omnientity.api.ModifyComponentDatagramAction;

/**
 *
 * @author angle
 */
@EqualsAndHashCode
@ToString(callSuper = true)
@RequiredArgsConstructor
public class BasicModifyComponentDatagramAction implements ModifyComponentDatagramAction {
    private final @Getter int componentID;
    private final @Getter Object fieldValue;
}