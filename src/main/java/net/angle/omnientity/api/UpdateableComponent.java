package net.angle.omnientity.api;

import com.google.common.collect.ImmutableList;
import java.util.List;

/**
 *
 * @author angle
 */
public interface UpdateableComponent<T> extends Component {
    @Override
    public default List<Class<? extends Component>> getComponentInterfaceList() {
        return ImmutableList.of(UpdateableComponent.class, Component.class);
    }
    public boolean hasUpdates();
    public void setHasUpdates(boolean hasUpdate);
    public void update(T update);
    public T getUpdate();
}
