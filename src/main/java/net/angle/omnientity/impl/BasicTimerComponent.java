package net.angle.omnientity.impl;

import lombok.Getter;
import net.angle.omnientity.api.Entity;
import net.angle.omnientity.api.TimerComponent;

/**
 *
 * @author angle
 */
public class BasicTimerComponent extends AbstractComponent implements TimerComponent {
    private @Getter float time;

    public BasicTimerComponent(float time, Entity entity) {
        super(entity);
        this.time = time;
    }

    public BasicTimerComponent(Entity entity) {
        this(0, entity);
    }

    @Override
    public void addTime(float time) {
        this.time += time;
    }
}
